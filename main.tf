terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}




terraform {
  backend "s3" {
    bucket = "revolgy-terraform"
    key    = "terraform/statefile.tfstate"
    region = "eu-central-1"
  }
}